# casper-ouija
A simple discord bot to play ouija

To start a ouija session use the command `Ouija,` followed by your question. Casper will start a ouija and will accept single character answers and block new questions in the same channel.

To end a ouija session someone else must type `goodbye` and the question in that channel will be ended.

You can also make questions using `Casper,` and Casper will return an answer. You can ask him to choose a random number from 1 to X for you, or decide between several items.

## invite
You can invite an already hosted Casper to your server with this [link](https://discordapp.com/api/oauth2/authorize?client_id=540398004566228993&permissions=75776&scope=bot)

## install and run
If you already have a bot [token](https://discordapp.com/developers/applications/) you can self-host Casper.

First install discord.js:
>npm install discord.js --save

Then run `node bot.js` and have fun!
If you want to set it up in a cloud host, it works flawlessly at [glitch.com](https://glitch.com).