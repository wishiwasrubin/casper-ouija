const Discord = require('discord.js');
const client = new Discord.Client();

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

class Question{
	constructor(Wizard, Question, channel){
		this.Wizard = Wizard;
		this.Question = Question;
		this.Spirit = null;
		this.Answer = new Array();
		this.channel = channel;
	}
}

var Ouija = false;
Questions = new Array();

client.on('message', (msg) => {
	var Current = Questions.find((obj) => {
		return obj.channel === msg.channel.id;
	});

	if (Current === undefined) {
		if (msg.content.toLowerCase().startsWith('ouija,')) {
			Ouija = new Question(msg.author.id, msg.content, msg.channel.id);
			Questions.push(Ouija);

			msg.channel.send('```Your question has been sent to the spirits.```');
		}
	} else {
		if (msg.channel.id === Current.channel) {
			if (msg.author.id != Current.Wizard && msg.author.id != Current.Spirit) {
				if (msg.content.toLowerCase().startsWith('ouija,')) {
					msg.reply('Wait your turn.');
				}

				if ((msg.content.length) < 2) {
					Current.Spirit = msg.author.id;
					if(msg.content === '_' || msg.content === '-') {
						Current.Answer.push(' ');
					} else {
						Current.Answer.push(msg.content);
					}
					msg.channel.send('`Your answer will be sent.`');
				}

				if (msg.content.toLowerCase().startsWith('goodbye')) {
					msg.channel.send('```Question: '+Current.Question+'\nAnswer: '+Current.Answer.join('')+'```');
					Questions.splice(Questions.findIndex((obj) => {
						obj.channel === msg.channel.id
					}), 1);
				}
			}
		}
	}
	
  	if (msg.content.toLowerCase().startsWith('casper, help.')) {
		msg.channel.send('Use `Ouija,` to ask the spirits a question.\nSay `-` or `_` to insert white space.\nSay `Goodbye.` to end the question.\n\nAsk me with `Casper,`');
	}

	if (msg.content.toLowerCase().startsWith('casper,') 
	&& msg.content.toLowerCase().endsWith('?')) {
		if (msg.content.toLowerCase().includes('how many') 
		|| msg.content.toLowerCase().includes('how much')) {
			CasperSays = ['A lot.', 'Not too much.', 'A fair amount.'];
		} else if (msg.content.toLowerCase().includes('how old')) {
			CasperSays = ['Too young.', 'Too old.', ];
		} else if (msg.content.toLowerCase().includes('is')) {
			CasperSays = ['Yes.', 'No.', 'Do you really want to know?'];
		} else if (msg.content.toLowerCase().includes('when')) {
			CasperSays = ['Soon.', 'Never.', 'Later.', 'Already happened.'];
		} else if (msg.content.toLowerCase().includes('how')) {
			CasperSays = ['I do not know.', 'With practice.', 'I cannot say.', 'It is not possible.'];
		} else if (msg.content.toLowerCase().includes('who')) {
			CasperSays = ['You.', 'Someone else.', 'A relative.', 'An unknown person.', 'The person watching you now.'];
		} else if (msg.content.toLowerCase().includes('where')) {
			CasperSays = ['At the very place you are now.', 'In your bedroom.', 'I cannot say.', 'You do not want to know.'];
		} else if (msg.content.toLowerCase().includes('should') || msg.content.toLowerCase().includes('will') || msg.content.toLowerCase().includes('do')) {
			CasperSays = ['Yes.', 'No.', 'Maybe.'];
		} else if (msg.content.toLowerCase().includes('or')) {
			CasperSays= msg.content.replace('Casper, ', '').replace('?', '').split(/or/);
		} else if (msg.content.toLowerCase().includes('1 to')) {
			var int = msg.content.toLowerCase().substr(-4).split(/(\d+)/);
			var max = int[1];
			var n = 1;
			CasperSays = [];
			while(n < max){
				CasperSays.push(n++);
			}
		} else {
			CasperSays = ['I cannot say.', 'I do not know.', 'Maybe.'];
		}
		msg.channel.send(CasperSays[Math.floor(Math.random()*CasperSays.length)])
	}

	if (msg.content.toLowerCase().includes('should i kill myself')) {
		msg.reply('asap.');
	}

	if (msg.content === 'Vera') {
		msg.channel.send('I love Vera.');
	}
});

client.login(process.env.BOT_TOKEN);